nodejs-maven-plugin
===================

A maven plugin that executes nodejs commands

##How to install:

### Repository
#### Use your own repository
Upload nodejs artifacts to your own repository or use Sirrapa's repositories.

##### Use the Sirrapa repositories
Add the following repositories to your pom.xml or settings.xml

      <repositories>
        <repository>
          <id>sirrapa-central</id>
          <url>https://nexus.sirrapa.com/content/groups/public/</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
        </repository>
        <repository>
          <id>sirrapa-central-snapshot</id>
          <url>https://nexus.sirrapa.com/content/groups/public-snapshots/</url>
          <layout>default</layout>
          <releases>
            <enabled>false</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
        </repository>
        </repositories>
      <pluginRepositories>
        <pluginRepository>
          <id>central</id>
          <url>https://nexus.sirrapa.com/content/groups/public/</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
          </snapshots>
        </pluginRepository>
      </pluginRepositories>

##### Configure your pom file:
    <project>
        <modelVersion>4.0.0</modelVersion>
        <groupId>sample</groupId>
        <artifactId>maven-sample</artifactId>
        <packaging>jar</packaging>
        <version>1.0-SNAPSHOT</version>
        <name>Sample pom</name>
        <build>
            <plugins>
                <plugin>
                    <groupId>com.sirrapa.maven.plugins</groupId>
                    <artifactId>nodejs-maven-plugin</artifactId>
                    <!-- Optional, if you want the js to compile when mvn:compile... -->
                    <executions>
                        <execution>
                            <id>compile-js</id>
                            <phase>process-resources</phase>
                            <goals>
                                <goal>compile</goal>
                            </goals>
                        </execution>
                    </executions>
                    <!-- Optional end -->
                    <configuration>
                        <!-- Optional, the values are default -->
                        <nodejsGroupId>org.nodejs</nodejsGroupId>
                        <nodejsArtifactId>nodejs</nodejsArtifactId>
                        <nodejsVersion>0.8.15</nodejsVersion>
                        <nodejsClassifier>${os.family}</nodejsClassifier>
                        <nodejsType>tar</nodejsType>
                        <!-- Optional end -->
                        <modules>
                            <module>
                                <name>--version</name>
                                <arguments>
                                    <argument>-o buildconfig.js</argument>
                                </arguments>
                            </module>
                        </modules>
                    </configuration>
                </plugin>
            </plugins>
        </build>
    </project>

##### Test it
<pre>
mvn nodejs:compile
</pre>

##Example
An example project is located at ://nodejs-maven-plugin/tree/master/src/test/resources/projects/project-001

If you want to the nodejs-maven-plugin always to run when for instance compiling, just add the following section to the plugin configuration:
    <plugin>
        <groupId>com.sirrapa.maven.plugins</groupId>
        <artifactId>nodejs-maven-plugin</artifactId>
        <executions>
            <execution>
                <id>compile-js</id>
                <phase>process-resources</phase>
                <goals>
                    <goal>compile</goal>
                </goals>
            </execution>
        </executions>
        <configuration>
            ...
        </configuration>
    </plugin>
