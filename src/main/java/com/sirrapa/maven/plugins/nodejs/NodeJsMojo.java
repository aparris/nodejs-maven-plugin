package com.sirrapa.maven.plugins.nodejs;

import java.io.File;

import com.sirrapa.maven.plugins.nodejs.config.NodeJsModule;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.dependency.AbstractDependencyMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.Os;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.cli.Arg;
import org.codehaus.plexus.util.cli.CommandLineException;
import org.codehaus.plexus.util.cli.CommandLineUtils;
import org.codehaus.plexus.util.cli.Commandline;

/**
 * Goal which runs Nodejs.
 */
@Mojo(name = "compile", requiresDependencyResolution = ResolutionScope.COMPILE,
        defaultPhase = LifecyclePhase.PROCESS_SOURCES, threadSafe = true)
public class NodeJsMojo extends AbstractDependencyMojo {


    /**
     * The nodejs file to run
     */
    @Parameter
    protected NodeJsModule[] modules;

    /**
     * The nodejs nodejsGroupId
     */
    @Parameter(defaultValue = "org.nodejs")
    protected String nodejsGroupId;
    /**
     * The nodejs nodejsArtifactId
     */
    @Parameter(defaultValue = "nodejs")
    private String nodejsArtifactId;
    /**
     * The nodejs nodejsVersion
     */
    @Parameter(defaultValue = "0.8")
    protected String nodejsVersion;
    /**
     * The nodejs nodejsType
     */
    @Parameter(defaultValue = "tar")
    protected String nodejsType;
    /**
     * The nodejs nodejsClassifier
     */
    @Parameter
    protected String nodejsClassifier;

    /**
     * Default location used for nodejs output
     */
    @Parameter(defaultValue = "${project.build.directory}/nodejs")
    private File workingDirectory;

    /**
     * Default location where nodejs will be extracted to and run from
     */
    @Parameter(defaultValue = "${java.io.tmpdir}/nodejs")
    protected File nodejsDirectory;

    /**
     * Execution of NodeJs
     */
    public void execute() throws MojoExecutionException {
        String nodeJsArtifactString = String.format("%s:%s:%s:%s:%s", nodejsGroupId, nodejsArtifactId, nodejsVersion, nodejsClassifier, nodejsType);
        try {
            initializeModulesConfiguration();

            //get the nodejs executable path
            String nodeJsExecutable = getNodeJsExecutable(nodejsClassifier);

            //only download and extract if it does not exists
            if (!FileUtils.fileExists(nodeJsExecutable)) {
                Artifact nodeJsArtifact = resolveNodeJsArtifact();

                if (nodeJsArtifact != null) {
                    unpackNodeJs(nodeJsArtifact, nodejsDirectory);
                }
            }

            //scream if it does not exists now....
            if (!FileUtils.fileExists(nodeJsExecutable)) {
                throw new MojoExecutionException(String.format("Unable to locate nodejs executable at '%s'",nodeJsExecutable));
            }
            executeNodeJs(nodeJsExecutable);
        } catch (ArtifactResolutionException e) {
            throw new MojoExecutionException("Unable to locate artifact: '" + nodeJsArtifactString + "' in any repository.", e);
        } catch (ArtifactNotFoundException e) {
            throw new MojoExecutionException("Unable to locate artifact: '" + nodeJsArtifactString + "' in any repository.", e);
        } catch (CommandLineException e) {
            throw new MojoExecutionException("Command execution failed.", e);
        } catch (NodeJsPluginException e) {
            throw new MojoExecutionException("", e);
        }
    }

    /**
     * Determine the right command executable for the given osFamily
     *
     * @param osFamily
     * @return
     */
    protected String getNodeJsExecutable(String osFamily) {
        if (osFamily == null) {
            throw new IllegalArgumentException("osFamily is null");
        }
        getLog().debug(String.format("Determing executable for osFamily = '%s'", osFamily));
        StringBuilder sb = new StringBuilder(nodejsDirectory.getAbsolutePath());
        if (osFamily.toLowerCase().startsWith("win") || Os.FAMILY_DOS.equals(osFamily)) {
            sb.append(File.separator).append("node.exe");
        } else {
            sb.append(File.separator).append("bin").append(File.separator).append("node");
        }
        getLog().info(String.format("Determined executable for osFamily '%s' = '%s'", osFamily, sb.toString()));
        return sb.toString();
    }

    /**
     * Executes the given commandline
     *
     * @param commandLine
     * @return
     * @throws CommandLineException
     */
    protected void executeCommandLine(Commandline commandLine)
            throws CommandLineException, MojoExecutionException {
        getLog().info("Executing command: " + commandLine.toString());
        CommandLineUtils.StringStreamConsumer systemErr = new CommandLineUtils.StringStreamConsumer();
        CommandLineUtils.StringStreamConsumer systemOut = new CommandLineUtils.StringStreamConsumer();

        int exitCode = CommandLineUtils.executeCommandLine(commandLine, systemOut, systemErr);
        String output = StringUtils.isEmpty(systemOut.getOutput()) ? null : '\n' + systemOut.getOutput().trim();
        if (StringUtils.isNotEmpty(output)) {
            getLog().info(output);
        }
        if (exitCode != 0) {
            output = StringUtils.isEmpty(systemErr.getOutput()) ? null : '\n' + systemErr.getOutput().trim();
            if (StringUtils.isNotEmpty(output)) {
                getLog().error(output);
            }
            throw new MojoExecutionException("Result of " + commandLine + " execution is: '" + exitCode + "'.");
        }
    }

    /**
     * Create an CommandLine for the given executable
     *
     * @param workDir
     * @param executable
     * @param moduleName
     * @param args
     * @return
     */
    protected Commandline getCommandLine(File workDir, String executable, String moduleName, String... args) {
        Commandline commandLine = new Commandline();
        commandLine.getShell().setQuotedExecutableEnabled(false);
        commandLine.getShell().setQuotedArgumentsEnabled(false);

        if (workDir != null) {
            if (!workDir.exists()) {
                workDir.mkdirs();
            }
            commandLine.setWorkingDirectory(workDir);
        }

        commandLine.setExecutable(executable);
        if (moduleName != null) {
            Arg arg = commandLine.createArg();
            arg.setValue(moduleName);
        }
        if (args != null) {
            commandLine.addArguments(args);

        }

        getLog().debug("commandLine = " + commandLine);

        return commandLine;
    }

    /**
     * Execute nodejs for every module in #modules.
     *
     * @param executable
     * @throws MojoExecutionException
     * @throws CommandLineException
     */
    protected void executeNodeJs(String executable) throws MojoExecutionException, CommandLineException {
        for (NodeJsModule module : modules) {
            // get a commandline with the nodejs executable
            Commandline commandLine = getCommandLine(workingDirectory, executable, module.getName(), module.getArguments());
            executeCommandLine(commandLine);
        }
    }

    /**
     * Unpacks the given artifact file to the given workingDirectory.
     *
     * @param nodeJsArtifact
     * @param outputDirectory
     * @throws MojoExecutionException
     */

    protected void unpackNodeJs(Artifact nodeJsArtifact, File outputDirectory) throws MojoExecutionException {
        unpack(nodeJsArtifact, outputDirectory);
    }

    /**
     * Resolve the nodejs artifact from the repos #remoteRepositories and #localRepository.
     *
     * @return The desired  Artifact file if resolved
     * @throws ArtifactNotFoundException   if artifact was not resolved
     * @throws ArtifactResolutionException if there went something wrong while resolving
     */
    protected Artifact resolveNodeJsArtifact() throws ArtifactNotFoundException, ArtifactResolutionException {
        //create desired nodejs artifact

        Artifact artifact = getFactory().createArtifactWithClassifier(nodejsGroupId, nodejsArtifactId, nodejsVersion, nodejsType, nodejsClassifier);

        //resolve artifact. This resolving process is not transitive!
        getResolver().resolve(artifact, getRemoteRepos(), getLocal());

        return artifact;
    }

    /**
     * Initializes the nodejs modules configuration.
     *
     * @throws NodeJsPluginException if the configuration is invalid
     */
    protected void initializeModulesConfiguration()
            throws NodeJsPluginException {
        nodejsClassifier = Os.OS_FAMILY;
        if (modules == null) {
            throw new NodeJsPluginException("No NodeJs modules configured.");
        }
    }


}
