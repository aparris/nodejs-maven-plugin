package com.sirrapa.maven.plugins.nodejs.config;/*
 * Copyright (c) KLM Royal Dutch Airlines. All Rights Reserved.
 * ============================================================
 *
 * User: x082085
 * Date: 12/4/12
 * Time: 8:21 AM
 */

public class NodeJsModule {
    public static final String NAME = "name";
    public static final String MODULE = "module";
    public static final String ARGUMENTS = "arguments";
    public static final String ARGUMENT = "argument";

    private String name;
    private String[] arguments;


    /**
     * Getter for property 'name'.
     *
     * @return Value for property name
     */
     public String getName() { return name; }

    /**
     * Getter for property 'arguments'.
     *
     * @return Value for property arguments
     */
     public String[] getArguments() { return arguments; }

}
