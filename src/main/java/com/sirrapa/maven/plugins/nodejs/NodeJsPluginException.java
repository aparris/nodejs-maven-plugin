package com.sirrapa.maven.plugins.nodejs;

/**
 * The base exception of the NodeJs plugin.
 */
public class NodeJsPluginException
    extends Exception
{

    public NodeJsPluginException()
    {
    }

    public NodeJsPluginException(String message)
    {
        super( message );
    }

    public NodeJsPluginException(Throwable cause)
    {
        super( cause );
    }

    public NodeJsPluginException(String message, Throwable cause)
    {
        super( message, cause );
    }
}
