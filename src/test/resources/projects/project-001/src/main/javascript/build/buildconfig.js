// docs
// https://github.com/jrburke/r.js/blob/master/build/example.build.js
({
    baseUrl     : "./",
    dir         : "${project.build.outputDirectory}/js",


    optimize    : "uglify",

    findNestedDependencies: true,

    preserveLicenceComments : true,

    modules : [
        {
            name : "dist/js/application"
        }
    ]

})

