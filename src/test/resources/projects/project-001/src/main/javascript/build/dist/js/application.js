define(function () {

    var Application = {
        name: '${project.name}',
        description: '${project.description}',
        version: '${project.version}'
    };

});