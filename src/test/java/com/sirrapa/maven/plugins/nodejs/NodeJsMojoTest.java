package com.sirrapa.maven.plugins.nodejs;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sirrapa.maven.plugins.nodejs.config.NodeJsModule;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.DefaultArtifactFactory;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.handler.manager.DefaultArtifactHandlerManager;
import org.apache.maven.artifact.repository.DefaultArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.plugin.testing.ArtifactStubFactory;
import org.apache.maven.plugin.testing.stubs.StubArtifactResolver;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.codehaus.plexus.logging.Logger;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.Os;
import org.codehaus.plexus.util.cli.Commandline;
import org.codehaus.plexus.util.cli.shell.BourneShell;
import org.codehaus.plexus.util.cli.shell.CmdShell;
import org.codehaus.plexus.util.cli.shell.CommandShell;

public class NodeJsMojoTest extends AbstractMojoTestCase {
    NodeJsMojo mojo;
    private final String localRepoDir = "target/local-repo/";
    private final String workDir = System.getProperty("java.io.tmpdir") + "nodejs/";
    private Logger log;

    @Override
    public void setUp() throws Exception {
        // required for mojo lookups to work
        super.setUp();

        log = container.getLogger();

        File dir;
        log.debug(">>>Cleaning nodejsDirectory " + getBasedir() + "/" + workDir + "...");
        dir = new File(workDir);
        FileUtils.deleteDirectory(dir);
        FileUtils.forceMkdir(dir);

        log.debug(">>>Cleaning local repo " + getBasedir() + "/" + localRepoDir + "...");
        dir = new File(getBasedir() + "/" + localRepoDir);
        FileUtils.deleteDirectory(dir);

    }

    private void loadMojo(String pomFileName) throws Exception {
        File pluginXml = new File(getBasedir(), "src/test/resources/" + pomFileName);
        assertTrue(String.format("The fil¬e %s does not exists", pluginXml.getAbsoluteFile()), pluginXml.exists());

        mojo = (NodeJsMojo) lookupMojo("compile", pluginXml);
        assertNotNull("Mojo is null after lookupMojo call", mojo);

        final DefaultArtifactRepository repository = new DefaultArtifactRepository("local",
                new File(getBasedir(), localRepoDir).toURI().toURL().toString(), new DefaultRepositoryLayout());
        repository.setBasedir(getBasedir() + localRepoDir);

        mojo.setLocal(repository);
        mojo.setFactory(new DefaultArtifactFactory());
        mojo.setResolver(new StubArtifactResolver(new ArtifactStubFactory(mojo.nodejsDirectory, true), false, false));

        DefaultArtifactHandlerManager artifactHandlerManager = new DefaultArtifactHandlerManager();
        setVariableValueToObject(artifactHandlerManager, "artifactHandlers", new HashMap());
        Map<String, ?> handlerDescriptors = getContainer().getComponentDescriptorMap(ArtifactHandler.ROLE);
        if (handlerDescriptors != null) {
            log.debug("Registering all unregistered ArtifactHandlers...");

            Set<String> existingHints = artifactHandlerManager.getHandlerTypes();
            if (existingHints != null) {
                for (String hint : existingHints) {
                    handlerDescriptors.remove(hint);
                }
            }

            if (handlerDescriptors.isEmpty()) {
                log.debug("All ArtifactHandlers are registered. Continuing...");
            } else {
                Map<String, ArtifactHandler> unregisteredHandlers = new HashMap<String, ArtifactHandler>(handlerDescriptors.size());

                for (String hint : handlerDescriptors.keySet()) {
                    try {
                        unregisteredHandlers.put(hint, (ArtifactHandler) lookup(ArtifactHandler.ROLE, hint));
                        log.debug("Adding ArtifactHandler for: " + hint);
                    } catch (ComponentLookupException e) {
                        mojo.getLog().warn("Failed to lookup ArtifactHandler with hint: " + hint + ". Reason: " + e.getMessage(), e);
                    }
                }

                artifactHandlerManager.addHandlers(unregisteredHandlers);
            }
        }
        setVariableValueToObject(mojo.getFactory(), "artifactHandlerManager", artifactHandlerManager);
    }

    public void testMojoLookup() throws Exception {
        loadMojo("plugin-config.xml");
    }


    public void testMissingModules() throws Exception {
        loadMojo("plugin-config-no-modules.xml");
        try {
            mojo.initializeModulesConfiguration();
            fail("An NodeJsPluginException was expected");
        } catch (NodeJsPluginException e) {
            //this is expected
            //e.printStackTrace();
            assertEquals("The exception is invalid", "No NodeJs modules configured.", e.getMessage());
        }
    }

    public void testMissingModule() throws Exception {
        loadMojo("plugin-config-no-module.xml");
        try {
            setVariableValueToObject(mojo, "modules", null);
            mojo.initializeModulesConfiguration();
            fail("An NodeJsPluginException was expected");
        } catch (NodeJsPluginException t) {
            //this is expected
            //t.printStackTrace();
            assertEquals("The exception message is invalid", "No NodeJs modules configured.", t.getMessage());
        }
    }

    public void testMissingModuleName() throws Exception {
        loadMojo("plugin-config-no-module-name.xml");
        try {
            mojo.initializeModulesConfiguration();
//            fail("An NodeJsPluginException was expected");
        } catch (NodeJsPluginException t) {
            //this is expected
            //t.printStackTrace();
            assertNotNull("Expected an exception cause.", t.getCause());
            Throwable e = t.getCause();
            String expected = IllegalArgumentException.class.getName();
            assertTrue(String.format("Expected %s as a cause.", expected), expected.equals(e.getClass().getName()));
            assertEquals("The causing exception message is invalid", "NodeJs module name not set.", e.getMessage());
        }
    }


    public void testUnpackNodeJs() throws Exception {
        loadMojo("plugin-config.xml");
        mojo.initializeModulesConfiguration();

        Artifact artifact = mojo.resolveNodeJsArtifact();
        assertNotNull("NodeJs artifact is null", artifact);

        setVariableValueToObject(mojo, "nodejsDirectory", new File(System.getProperty("java.io.tmpdir"), "nodejs/"));
        File tmpDir = new File(System.getProperty("java.io.tmpdir"), "nodejs");
        try {
            tmpDir.mkdirs();
            mojo.unpackNodeJs(artifact, tmpDir);

            String nodejsEx = mojo.getNodeJsExecutable(mojo.nodejsClassifier);

            assertTrue(String.format("Extract of nodejs to '%s' failed", tmpDir.getAbsolutePath()), tmpDir.exists());

        } finally {
            tmpDir.delete();
        }
    }

    public void testGetNodeJsExecutable() throws Exception {
        loadMojo("plugin-config.xml");
        mojo.initializeModulesConfiguration();

        setVariableValueToObject(mojo, "nodejsDirectory", new File(System.getProperty("java.io.tmpdir") + "nodejs/"));
        String nodejsEx;

        for (String family : Os.getValidFamilies()) {
            nodejsEx = mojo.getNodeJsExecutable(family);
            log.debug(String.format("nodejsEx for %s = %s", family, nodejsEx));

            if (family.startsWith("win")|| Os.FAMILY_DOS.equals(family)) {
                assertTrue(String.format("The nodejs executable for %s family is invalid", family), nodejsEx.endsWith(File.separator + "nodejs" + File.separator + "node.exe"));
            } else {
                assertTrue(String.format("The nodejs executable for %s family is invalid", family), nodejsEx.endsWith(File.separator + "nodejs" + File.separator + "bin" + File.separator + "node"));
            }
        }
    }


    public void testGetCommandLine() throws Exception {
        loadMojo("projects/project-001/pom.xml");
        mojo.initializeModulesConfiguration();
        String nodejsDir = "/tmp/nodejs";
        String workDir = "/work";
        setVariableValueToObject(mojo, "nodejsDirectory", new File(nodejsDir + "/"));

        assertNotNull("Mojo modules is null", mojo.modules);
        assertTrue("Mojo modules should contain at least one module", mojo.modules.length == 1);
        NodeJsModule module = mojo.modules[0];
        String executable = mojo.getNodeJsExecutable("win");
        assertNotNull("executable is null", executable);

        Commandline cmd = mojo.getCommandLine(new File(workDir + "/"), executable, module.getName(), module.getArguments());
        assertNotNull("commandline is null", cmd);
        /** the expected command shell differs per OS... */
        //faking windows OS...
        cmd.setShell(new CmdShell());
        cmd.getShell().setQuotedExecutableEnabled(false);
        cmd.getShell().setQuotedArgumentsEnabled(false);
        String expected = "cmd.exe /X /C \"%s %s -o ${project.build.outputDirectory}/build/buildconfig.js\"";
        assertEquals("The windows commandline string is invalid", String.format(expected, executable, module.getName()), cmd.toString());

        //faking all OS with bourne shell...
        cmd.setShell(new BourneShell());
        cmd.getShell().setQuotedExecutableEnabled(false);
        cmd.getShell().setQuotedArgumentsEnabled(false);
        expected= "/bin/sh -c cd %s && %s %s -o ${project.build.outputDirectory}/build/buildconfig.js";
        assertEquals("The commandline string is invalid", String.format(expected, cmd.getWorkingDirectory().getAbsolutePath(), executable, module.getName()), cmd.toString());

     }

//    public void testCmdLine() throws Exception {
//                   mojo = new NodeJsMojo();
//        Commandline cmd = mojo.getCommandLine(new File("/opt/node-plugin/work/"),"/opt/node-plugin/nodejs/bin/node","--version");
//                   mojo.executeCommandLine(cmd);
//    }
}
